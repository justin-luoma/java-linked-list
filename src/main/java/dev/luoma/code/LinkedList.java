package dev.luoma.code;

import java.util.Objects;

public class LinkedList {
  private Node head;

  public LinkedList() {
    this.head = null;
  }

  public LinkedList(Object... items) {
    Node node = null;
    for (Object item : items) {
      if (node == null) {
        node = new Node(item);
        this.head = node;
      } else {
        node.next = new Node(item);
        node = node.next;
      }
    }
  }

  public boolean isEmpty() {
    return head == null;
  }

  public Object get(int index) {
    int i = 0;
    if (this.head == null) {
      return null;
    }
    Node cur = this.head;
    while (index != i) {
      cur = cur.next;
      ++i;
    }
    return cur.data;
  }

  public void add(Object item) {
    Node cur = this.head;
    Node i = new Node(item);
    if (cur == null) {
      cur = i;
      this.head = i;
    } else {
      while (cur.next != null) {
        cur = cur.next;
      }
      cur.next = i;
    }
  }

  public int size() {
    if (this.head == null) {
      return 0;
    }
    int size = 0;
    Node cur = this.head;
    while (cur != null) {
      cur = cur.next;
      ++size;
    }
    return size;
  }

  public Object pop() {
    if (this.head == null) {
      return null;
    }
    if (this.head.next == null) {
      Object data = this.head.data;
      this.head = null;
      return data;
    }
    Node prev = null;
    Node cur = this.head;
    while (true) {
      if (cur.next == null) {
        prev.next = null;
        return cur.data;
      }
      prev = cur;
      cur = cur.next;
    }
  }

  public boolean contains(Object item) {
    if (this.isEmpty()) {
      return false;
    }
    boolean contains = false;
    Node cur = this.head;
    while (cur != null) {
      if (cur.data.equals(item)) {
        return true;
      }
      cur = cur.next;
    }
    return false;
  }

  class Node {
    private Object data;
    private Node next;

    Node(Object item) {
      this.data = item;
      this.next = null;
    }
  }
}
