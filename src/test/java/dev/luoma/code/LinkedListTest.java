package dev.luoma.code;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LinkedListTest {
  private LinkedList list;

  @BeforeEach
  void init() {
    this.list = new LinkedList();
  }

  @Test
  void isEmptyTrueWhenEmptyTest() {
    assertTrue(list.isEmpty());
  }

  @Test
  void getReturnsItemTest() {
    int zero = 0;
    int one = 1;
    int two = 2;
    int three = 3;

    LinkedList list = new LinkedList(zero, one, two, three);
    assertEquals(zero, list.get(0));
    assertEquals(one, list.get(1));
    assertEquals(two, list.get(2));
    assertEquals(three, list.get(3));
  }

  @Test
  void getReturnsNullTest() {
    assertNull(this.list.get(0));
  }

  @Test
  void addAddsOneEmptyListTest() {
    assertTrue(this.list.isEmpty());
    int one = 1;
    this.list.add(one);
    assertFalse(this.list.isEmpty());
    assertEquals(one, this.list.get(0));
  }

  @Test
  void addAddsToEndTest() {
    LinkedList list = new LinkedList(0, 1, 2, 3, 4);
    list.add(5);
    assertEquals(6, list.size());
    assertEquals(5, list.get(5));
  }

  @Test
  void emptySizeReturnsZeroTest() {
    assertEquals(0, this.list.size());
  }

  @Test
  void sizeReturnsSizeTest() {
    this.list.add(1);
    assertEquals(1, this.list.size());
    this.list.add(2);
    assertEquals(2, this.list.size());
  }

  @Test
  void popEmptyListReturnsNull() {
    assertNull(this.list.pop());
  }

  @Test
  void popRemovesLastItemTest() {
    this.list.add(1);
    int one = (Integer) this.list.pop();
    assertEquals(1, one);
    assertNull(this.list.pop());
  }

  @Test
  void popRemovesOneItem() {
    this.list.add(1);
    this.list.add(2);
    assertEquals(2, this.list.pop());
    assertEquals(1, this.list.get(0));
  }

  @Test
  void containsReturnsTrueTest() {
    this.list.add(1);
    assertTrue(this.list.contains(1));
  }

  @Test
  void containsFalseEmptyListTest() {
    assertFalse(this.list.contains(1));
  }

  @Test
  void containsMultipleReturnsTrueTest() {
    this.list.add(1);
    this.list.add(2);
    this.list.add(3);
    assertTrue(this.list.contains(2));
    assertTrue(this.list.contains(3));
  }

  @Test
  void containsMultipleReturnsFalseTest() {
    this.list.add(1);
    this.list.add(2);
    this.list.add(3);
    assertFalse(this.list.contains(4));
  }
}
